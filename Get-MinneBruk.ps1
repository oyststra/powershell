Write-Host "Totalt minnebruk for $env:COMPUTERNAME"

# Hent ut et objekt om operativsystemet 
$ubrukt = Get-WmiObject win32_operatingsystem

# Hent ut et objekt om maskinen 
$totalt = Get-WmiObject win32_computersystem 

# Fra operativsystem-objektet hentes det ut fritt minne 
$ubrukt = $ubrukt.freephysicalmemory 
# Fra maskin-objektet hentes ut totalt minne på maskin 
$totalt = $totalt.totalphysicalmemory 

# a inneholder verdi i kb og b inneholder i byte, derfor må det deles på 1mb og 1gb for å få samme verdi på a og b 
$ubrukt = $ubrukt/1MB
$totalt = $totalt/1GB

# Regn ut minnebruk i prosent 
$sum = $totalt-$ubrukt 
$sum = [double] (($sum * 100) / ($totalt))

# Ta bort verdier etter komma 
$sum = "{0:N0}" -f $sum 

# Skriv ut prosentsummen 
[string] ($sum + "%")