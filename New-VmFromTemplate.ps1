﻿ [cmdletbinding()]
 param(
    [Parameter( 
        Position = 0,
        Mandatory = $true
    )]
    [ValidateNotNullOrEmpty()]
    [string]$vhdPath,

    [Parameter(
        Position = 1,
        Mandatory = $false
    )]
    [string]$vmName,
    
    [Parameter(
        Position = 2,
        Mandatory = $false
    )][string[]]$switchName,
    
    [parameter(
        Position = 3,
        Mandatory = $true
    )]
    [ValidateNotNullOrEmpty()]
    [string]$parentPath
)

# Opprett virtuell disk
New-VHD -ParentPath $parentPath -Path $vhdPath

# Opprett virtuell maskin
New-VM -Name $vmName -VHDPath $vhdPath 

# Legg til svitsjer 
foreach ($switch in $additionalSwitches) {
    Add-VMNetworkAdapter -VMName $vmName -SwitchName $switch
}
